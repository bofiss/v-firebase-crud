import { initializeApp } from 'firebase'

const config = {
    apiKey: "AIzaSyC7R3ed6YCq7ST-rNlQkyb-fBo9Ps_y52s",
    authDomain: "vue-crud-b96b6.firebaseapp.com",
    databaseURL: "https://vue-crud-b96b6.firebaseio.com",
    projectId: "vue-crud-b96b6",
    storageBucket: "vue-crud-b96b6.appspot.com",
    messagingSenderId: "468028436253"
}
const app = initializeApp(config)


export const db = app.database()
export const namesRef = db.ref('names')